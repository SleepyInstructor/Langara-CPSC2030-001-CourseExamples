/* for this example we want to find the distance between 
stores, so we need a self join, and then compute the distance
-We have self-join
-and math to compute a column
-we also elimiated the case we calculate the distance to the same loaction
-notice that there are still 2 entries per a store combo. 
-Not going to worry about that, we can consider it vector quantity ^_^
 */


select s1.store, s2.store, SQRT(Pow((s1.loc_x-s2.loc_x),2)+Pow((s1.loc_y-s2.loc_y),2)) as distance
from stores as s1, stores as s2
where not (s1.store = s2.store);