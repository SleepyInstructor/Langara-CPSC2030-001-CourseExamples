console.log("Script is connected");
let characters = [
    {
        src: "http://valkyria.sega.com/img/character/img01.png",
        name: "Claude Wallace",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad E",
        rank: "First Lieutenant",
        role: "Tank Commander",
        class: "Scout ",
        des: "Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss02.jpg",
        name: "Riley Miller",
        side: "Edinburgh Army",
        unit: "Federate Joint Ops",
        rank: "Second Lieutenant",
        role: "Artillery Advisor",
        class: "Grenadier",
        des: "Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss03.jpg",
        name: "Raz",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad E",
        rank: "Sergeant",
        role: "Fireteam Leader",
        class: "Shocktrooper ",
        des: "Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss04.jpg",
        name: "Kai Schulen",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad E",
        rank: "Sergeant Major",
        role: "Fireteam Leader",
        class: "Sniper",
        des: "Born in the Gallian city of Hafen, this cool and collected sharpshooter has earned the codename 'Deadeye Kai.' Along with her childhood friends, she joined a foreign military to take the fight to the Empire. She loves fresh-baked bread, almost to a fault."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss11.jpg",
        name: "Minerva Victor ",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad F",
        rank: "First Lieutenant",
        role: "Senior Commander",
        class: "Scout",
        des: "Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss12.jpg",
        name: "Karen Stuart",
        side: "Edinburgh Army",
        unit: "Squad E",
        rank: "Corporal",
        role: "Combat EMT",
        class: "Medic",
        des: "Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss13.jpg",
        name: "Ragnarok",
        side: "Edinburgh Army",
        unit: "Squad E",
        rank: "K-9 Unit",
        role: "Mascot",
        class: "Medic",
        des: "Once a stray, this good good boy is lovingly referred to as 'Rags.'As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss15.jpg",
        name: "Miles Arbeck",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad E",
        rank: "Sergeant",
        role: "Tank Operator",
        class: "Driver",
        des: "Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss16.jpg",
        name: "Dan Bentley",
        side: "Edinburgh Army",
        unit: "Ranger Corps, Squad E",
        rank: "Private First Class",
        role: "APC Operator",
        class: "Driver",
        des: "Born in the United States of Vinland, this driver loves armored personnel carriers with a passion. His skill behind the wheel is matched only by his way with a wrench. Though not much of a talker, he takes pride in carrying his teammates through combat."
    },
    {
        src: "http://valkyria.sega.com/img/character/chara-ss16.jpg",
        name: "Ronald Albee",
        side: "Edinburgh Army",
        unit: "Ranger Crops, Squad F",
        rank: "Second Lieutenant",
        role: "Tank Operator",
        class: "Driver",
        des: "Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code."
    }
]
window.addEventListener("load",setup);

function updateProfile(member){
    
    document.querySelector(".profile>img").src = member.src;
    document.querySelector(".profile .name").innerHTML = member.name;
    document.querySelector(".profile .side").innerHTML = member.side;
    document.querySelector(".profile .unit").innerHTML = member.unit;
    document.querySelector(".profile .rank").innerHTML = member.rank;
    document.querySelector(".profile .role").innerHTML = member.role;
    document.querySelector(".profile .class").innerHTML = member.class;
    document.querySelector(".profile .des").innerHTML = member.des;
}
function createUpdateProfileListener(member){
    return function(event){        
        updateProfile(member);
    }
}
function removeMember(event){
    event.currentTarget.remove();
}
const maxMembers = 5;
function createSquadListAddListener(member){
    return function(event){
      //check if member is already in unit list
      let entrylist = document.querySelectorAll(".squad div.member");
      if(entrylist.length >= 5) {
          return;
      }
      for(let element of entrylist){
          if(element.innerHTML == member.name){
              return;
          }
      }
      //if not add new element to list
      let element = document.createElement("div");
      element.classList.add("member")
      element.addEventListener("click", removeMember);
      element.addEventListener("mouseover", createUpdateProfileListener(member));
      element.innerHTML = member.name;
      document.querySelector(".container>.squad").appendChild(element); 
      
    }
}

function setup(){
    let memberList = document.querySelector(".container>.unit");
    for(let member of characters){
        let element = document.createElement("div");
   
        element.addEventListener("click", createSquadListAddListener(member));
        element.addEventListener("mouseover", createUpdateProfileListener(member));
        element.innerHTML = member.name;
        memberList.appendChild(element); 
               
    }
}