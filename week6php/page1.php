
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.0.2/less.min.js" ></script>
    <title>Document</title>
</head>
<?php
//Setup
include "sqlhelper.php";
//database stuff first
   //SQL  portion
   $user = 'store';
   $pwd = 'store';
   $server = 'localhost';
   $dbname = 'store_example';

   $conn = new mysqli($server, $user, $pwd, $dbname);
   $result = $conn->query("call store_list()");
   clearConnection($conn);
   //Generate table in next blocks

?>
<body>
<!-- we are embedding php in html this week, but
    next week we will now better -->
    
Please choose a store:
<?php //Insert data here
   if($result){
    $table = $result->fetch_all(MYSQLI_ASSOC); //returns a table or rows. Rows are associative arrays
                                               
    foreach( $table as $row){
        $link = "page2.php?store=".urlencode($row["store"]); //use urlencode to change special characters to the right format
        echo "<div><a href= '$link'>";  // Manually generate the link tags
        echo $row["store"];
        echo "</a></div>";
    }

   } else {
       echo "Error";
   }

?>    

 
</body>
</html>

