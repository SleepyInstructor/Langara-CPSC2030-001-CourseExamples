function createClickHandler(menuElement) {

    let element = menuElement;  //copying reference for no reson, just show that I can
    const expandClass = "expand";
    //Event handler
    return  (event) =>{
        if (event.target == element.querySelector(".title")) {
            if (menuElement.className == expandClass) {
                menuElement.className = "";
            } else {
                menuElement.className = expandClass;
            }
        }
    }
}

function setupMenu(containerName) {
    let menus = document.querySelectorAll(containerName + ">div");

    menus.forEach((element)=>{
        element.onclick = createClickHandler(element);
    });
    
}
setupMenu(".menu");