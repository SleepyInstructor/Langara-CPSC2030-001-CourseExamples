-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 28, 2018 at 03:05 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chat`
--
CREATE DATABASE IF NOT EXISTS `chat` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `chat`;

DELIMITER $$
--
-- Procedures
--
DROP PROCEDURE IF EXISTS `get_last_hour_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_last_hour_messages` ()  NO SQL
select * from (select   *, TIMESTAMPDIFF(MINUTE,time,CURRENT_TIMESTAMP) as diff from messages 
where TIMESTAMPDIFF(MINUTE, time, CURRENT_TIMESTAMP) < 60
ORDER BY idx DESC
LIMIT 10) as tb
ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `get_latest_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_latest_messages` (IN `latest_time` DATETIME)  NO SQL
select * from messages 
WHERE time > latest_time
ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `get_messages`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_messages` ()  NO SQL
select * from messages ORDER BY idx ASC$$

DROP PROCEDURE IF EXISTS `insert_message`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `insert_message` (IN `user` TEXT, IN `msg` TEXT)  NO SQL
insert into messages (usr, message, time) 
values( user, msg, CURRENT_TIMESTAMP)$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

DROP TABLE IF EXISTS `messages`;
CREATE TABLE `messages` (
  `idx` int(11) NOT NULL,
  `usr` text NOT NULL,
  `message` text NOT NULL,
  `time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD UNIQUE KEY `idx` (`idx`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `idx` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
