<?php
require_once("dblib.php");

$conn = connectToDB();  //Will terminate if it cannot get connection

$queryString = "";
if(array_key_exists("time", $_POST)){
    $param = $conn->escape_string($_POST["time"]);
    $queryString = "call get_latest_messages('$param')";
} 

$result = $conn->query($queryString);
if($result){
   echo json_encode($result->fetch_all( MYSQLI_ASSOC));
} else {
    echo json_encode(array("error"=>"Could not fetch data"));
}
?>