<?php
require_once("dblib.php");

$conn = connectToDB();  //Will terminate if it cannot get connection

$queryString = "call get_last_hour_messages()";
$result = $conn->query($queryString);
if($result){
    echo json_encode($result->fetch_all( MYSQLI_ASSOC));
} else {
    echo json_encode(array("error"=>"Could not fetch data"));
}
?>