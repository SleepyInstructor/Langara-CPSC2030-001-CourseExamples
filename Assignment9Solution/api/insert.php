<?php
require_once("dblib.php");

$conn = connectToDB();  //Will terminate if it cannot get connection

if(!array_key_exists("user", $_POST) || !array_key_exists("message", $_POST)){
    echo json_encode(array("error"=>"Invalid input"));
    exit;
} 
$user = $conn->escape_string($_POST["user"]);
$message = $conn->escape_string($_POST["message"]);
$queryString = "call insert_message('$user','$message')";

$result = $conn->query($queryString);

if($result){
    echo json_encode(array("success"=>"message inserted"));
} else {
    echo json_encode(array("error"=>"unknown sql error"));
}

?>