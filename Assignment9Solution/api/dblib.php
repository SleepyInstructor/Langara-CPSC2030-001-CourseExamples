<?php
function clearConnection($mysql){
    while($mysql->more_results()){
       $mysql->next_result();
       $mysql->use_result();
    }
}

function connectToDB(){
    $server = 'localhost';
    $username = 'CPSC2030';
    $password = 'CPSC2030';
    $dbname = 'chat';
    return connect($server, $username, $password, $dbname);
}

function connect($server, $user, $pwd, $dbname){
    $conn = new mysqli($server, $user, $pwd, $dbname);
    if($conn->connect_errno){
       echo "Error: Failed to make a MySQL connection, here is why: \n";
       echo "Errno: " . $conn->connect_errno . "\n";
       echo "Error: " . $conn->connect_error . "\n";
    
       // You might want to show them something nice, but we will simply exit
       exit;
    }
    return $conn;
}
?>