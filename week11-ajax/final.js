/*global Request, fetch*/
let currentUser;
let currentIdx;

function createDiv(row) {
    let msg = "";

    msg = "<b>From:</b> " + row.from + "<br>" +
        "<b>Date</b>F" + row.date + "<br>" +
        "<b>Subject:</b>" + row.subject;

    msg = "<div class=\"indexItem\" idx=\"" + row.idx + "\">" + msg + "</div>";
    console.log(row);
    return msg;
}
let messageIndex = [];
$(".item").click(function(event) {

    //choose selected item
    $(".item").removeClass("selected");
    $(event.target).addClass("selected");

    let user = $(event.target).attr("user");
    currentUser = user;
    let req = new Request('getmaildirectory.php', {
        method: 'POST',
        headers: {

            'Content-Type': 'application/x-www-form-urlencoded'
        },
        body: `user=${user}`
    });

    fetch(req).then(function(response) {
        return response.json();
    }).then(function(msg) {

       messageIndex = msg;
       let output = "";
       for (let i = 0; i < messageIndex.length; i++) {
         output += createDiv(messageIndex[i]);
       }
       $(".index").html(output);

      })
    });

$(".index").click(function(event) {

    if ($(event.target).hasClass("indexItem")) {
        //only responds to click of items, not other places
        $(".indexItem").removeClass("selected");
        $(event.target).addClass("selected");
        let currentIdx = $(event.target).attr("idx");
        let req = new Request('getmail.php', {
           method: 'POST',
           headers: {

            'Content-Type': 'application/x-www-form-urlencoded'
           },
           body: `idx=${currentIdx}`
         })
    fetch(req).then(function(response){
        return response.json();
    }).then(function(data){
               $("span.to").html(data.to);
               $("span.from").html(data.from);
               $("span.date").html(data.date);
               $("span.subject").html(data.subject);
               $("span.message").html(data.message);
    })
    }
});
