//Setup expanding menu
const selectedClass = "selected";
$(".menu.clickable>.folder>.name").click(function (event) {
    let jElement = $(event.target);
    jElement.toggleClass(selectedClass);
    
})

//Setup for sliding
//This is a lttle different because the target
//may not be the menu itself
//and if we have more than one menu we are in trouble.

//So we will cheat and do this all in a closure.

const openClass = "open";
let allSldingMenus = $(".menu.sliding"); //we may have more than one.


function attachHandler(menu) {
    let jMenu = $(menu);
    jMenu.click(function (event) {
        jMenu.toggleClass(openClass);
    })
}
allSldingMenus.each(function(index,element) {
    attachHandler(element);
});