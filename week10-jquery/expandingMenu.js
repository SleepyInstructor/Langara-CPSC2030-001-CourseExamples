const openClass = "selected";
let allSldingMenus = $(".bookshelf>.book"); //we may have more than one.


function attachHandler(menu) {
    let jMenu = $(menu);
    jMenu.click(() => {
        allSldingMenus.removeClass(openClass);
        jMenu.addClass(openClass);
    })
}
allSldingMenus.toArray().forEach(element => {
    attachHandler(element);
});